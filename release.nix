{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, compiler ? "ghc966"
}:

let
  # There must be a better way than this! I just want to avoid
  # excessive compilation when using a non-standard compiler.
  #
  isDefaultCompiler = compiler == "ghc8107";

  gitignore = pkgs.nix-gitignore.gitignoreSourcePure [ ./.gitignore ];

  myHaskellPackages = pkgs.haskell.packages.${compiler}.override {
    overrides = hself: hsuper: {
      "swish" =
        hself.callCabal2nix "swish" (gitignore ./.) {};
    };
  };

  shell = myHaskellPackages.shellFor {
    packages = p: [
      p."swish"
    ];
    buildInputs = [
      pkgs.haskellPackages.cabal-install
      pkgs.haskellPackages.hlint
      pkgs.niv
    ] ++ pkgs.lib.optionals isDefaultCompiler [
      pkgs.haskellPackages.haskell-language-server
    ];
    withHoogle = isDefaultCompiler;
  };

  exe = pkgs.haskell.lib.justStaticExecutables (myHaskellPackages."swish");

in
{
  inherit shell;
  inherit exe;
  inherit myHaskellPackages;
  "swish" = myHaskellPackages."swish";
}
